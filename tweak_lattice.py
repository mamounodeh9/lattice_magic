# Another lattice addon, this time inspired by https://twitter.com/soyposmoderno/status/1307222594047758337

# This one lets you create an empty hooked up to a Lattice to deform all selected objects.
# A root empty is also created that can be (manually) parented to a rig in order to use this for animation.

# TODO:
# Merge this and Camera Lattice into one addon
# Re-organize the code

# Automagically determining the initial radius based on the selected objects' bounding box would be interesting

# More things:
# Mirror selected positions(along any axis, with pickable left/right up/down front/back preference)
# Grow selection

# Add operators to the UI wherever possible.

import bpy
from bpy.props import FloatProperty, IntVectorProperty, FloatVectorProperty, BoolProperty, PointerProperty, StringProperty, EnumProperty
from typing import List
from mathutils import Vector
from rna_prop_ui import rna_idprop_ui_create

from .utils import clamp, get_lattice_vertex_index, simple_driver, bounding_box_center_of_objects

coll_name = 'Tweak Lattices'

def ensure_tweak_lattice_collection(scene: bpy.types.Scene) -> bpy.types.Collection:
	coll = bpy.data.collections.get(coll_name)
	if not coll:
		coll = bpy.data.collections.new(coll_name)
		scene.collection.children.link(coll)

	return coll

def ensure_falloff_vgroup(
		lattice_ob: bpy.types.Object, 
		vg_name="Group", multiplier=1) -> bpy.types.VertexGroup:
	lattice = lattice_ob.data
	res_x, res_y, res_z = lattice.points_u, lattice.points_v, lattice.points_w

	vg = lattice_ob.vertex_groups.get(vg_name)

	center = Vector((res_x/2, res_y/2, res_z/2))
	max_res = max(res_x, res_y, res_z)

	if not vg:
		vg = lattice_ob.vertex_groups.new(name=vg_name)
	for x in range(res_x-4):
		for y in range(res_y-4):
			for z in range(res_z-4):
				index = get_lattice_vertex_index(lattice, (x+2, y+2, z+2))

				coord = Vector((x+2, y+2, z+2))
				distance_from_center = (coord-center).length
				influence = 1 - distance_from_center / max_res * 2
				
				vg.add([index], influence * multiplier, 'REPLACE')
	return vg

def add_radius_constraint(obj, hook, target):
	trans_con = obj.constraints.new(type='TRANSFORM')
	trans_con.target = target
	trans_con.map_to = 'SCALE'
	trans_con.mix_mode_scale = 'MULTIPLY'
	for prop in ['to_min_x_scale', 'to_min_y_scale', 'to_min_z_scale']:
		simple_driver(trans_con, prop, hook, '["Radius"]')
	return trans_con

def get_objects_of_lattice(hook: bpy.types.Object) -> List[bpy.types.Object]:
	objs = []
	ob_count = 0
	ob_prop_name = "object_"+str(ob_count)
	while ob_prop_name in hook:
		objs.append(hook[ob_prop_name])
	return objs

def add_objects_to_lattice(
		hook: bpy.types.Object, 
		objects: List[bpy.types.Object]):
	lattice_ob = hook['Lattice']
	
	# Check for existing
	offset = 0
	while "object_"+str(offset) in hook:
		offset += 1

	for i, o in enumerate(objects):
		o.select_set(False)
		if o.type!='MESH' or o in hook.values(): 
			offset -= 1
			continue
		m = o.modifiers.new(name="Tweak Lattice", type='LATTICE')
		m.object = lattice_ob
		hook["object_"+str(i+offset)] = o
		# Add driver to the modifier influence
		simple_driver(m, 'strength', hook, '["Tweak Lattice"]')

def remove_all_objects_from_lattice(hook: bpy.types.Object) -> List[bpy.types.Object]:
	lattice_ob = hook['Lattice']
	objs = []		
	ob_count = 0
	ob_prop_name = "object_"+str(ob_count)
	while ob_prop_name in hook:
		ob = hook[ob_prop_name]
		for m in ob.modifiers:
			if m.type!='LATTICE': continue
			if m.object == lattice_ob:
				m.driver_remove('strength')
				ob.modifiers.remove(m)
				break
		ob_count += 1
		objs.append(ob)
		del hook[ob_prop_name]
		ob_prop_name = "object_"+str(ob_count)
	return objs

def remove_objects_from_lattice(hook, objects):
	new_objs = []
	prev_objs = remove_all_objects_from_lattice(hook)
	for o in prev_objs:
		if o not in objects:
			new_objs.append(o)
	add_objects_to_lattice(hook, new_objs)

class TWEAKLAT_OT_Create(bpy.types.Operator):
	"""Create a lattice setup at the 3D cursor to deform selected objects"""
	bl_idname = "lattice.create_tweak_lattice"
	bl_label = "Create Tweak Lattice"
	bl_options = {'REGISTER', 'UNDO'}

	radius: FloatProperty(name="Radius", default=0.1, min=0, soft_max=0.2)
	resolution: IntVectorProperty(name="Resolution", default=(12, 12, 12), min=6, max=64)

	@classmethod
	def poll(cls, context):
		for ob in context.selected_objects:
			if ob.type=='MESH':
				return True
		return False

	def execute(self, context):
		scene = context.scene

		# Ensure a collection to organize all our objects in.
		coll = ensure_tweak_lattice_collection(context.scene)

		# Create a lattice object at the 3D cursor.
		lattice_name = "LTC-Tweak"
		lattice = bpy.data.lattices.new(lattice_name)
		lattice_ob = bpy.data.objects.new(lattice_name, lattice)
		coll.objects.link(lattice_ob)
		lattice_ob.hide_viewport = True

		# Set resolution
		lattice.points_u, lattice.points_v, lattice.points_w, = self.resolution
		lattice.points_u = clamp(lattice.points_u, 6, 64)
		lattice.points_v = clamp(lattice.points_v, 6, 64)
		lattice.points_w = clamp(lattice.points_w, 6, 64)

		# Create a falloff vertex group
		vg = ensure_falloff_vgroup(lattice_ob, vg_name="Hook")

		# Create an Empty at the 3D cursor
		hook_name = "Hook_"+lattice_ob.name
		hook = bpy.data.objects.new(hook_name, None)
		hook.empty_display_type = 'SPHERE'
		hook.empty_display_size = 0.5
		coll.objects.link(hook)

		# Create some custom properties
		hook['Lattice'] = lattice_ob
		hook['Multiplier'] = 1.0

		rna_idprop_ui_create(
			hook, "Tweak Lattice", default = 1.0,
			min = 0, max = 1,
			description = "Influence of this lattice on all of its target objects",
		)
		rna_idprop_ui_create(
			hook, "Radius", default = self.radius,
			min = 0, soft_max = 0.2, max=100,
			description = "Size of the influenced area",
		)

		# Create a Root Empty to parent both the hook and the lattice to.
		# This will allow pressing Ctrl+G/R/S on the hook to reset its transforms.
		root_name = "Root_" + hook.name
		root = bpy.data.objects.new(root_name, None)
		root.empty_display_type = 'CUBE'
		root.empty_display_size = 0.5
		if context.scene.tweak_lattice_location=='CENTER':
			meshes = [o for o in context.selected_objects if o.type=='MESH']
			root.matrix_world.translation = bounding_box_center_of_objects(meshes)
		else:
			root.matrix_world = context.scene.cursor.matrix
		coll.objects.link(root)
		root.hide_viewport = True
		hook['Root'] = root

		# Parent the root
		scene = context.scene
		matrix_backup = root.matrix_world.copy()
		root.parent = scene.tweak_lattice_parent_ob
		if root.parent and root.parent.type=='ARMATURE':
			bone = root.parent.pose.bones.get(scene.tweak_lattice_parent_bone)
			if bone:
				root.parent_type = 'BONE'
				root.parent_bone = bone.name
				root.matrix_world = matrix_backup

		# Parent lattice and hook to root
		lattice_ob.parent = root

		hook.parent = root

		# Add Hook modifier to the lattice
		hook_mod = lattice_ob.modifiers.new(name="Hook", type='HOOK')
		hook_mod.object = hook
		hook_mod.vertex_group = vg.name

		# Add Lattice modifier to the selected objects
		add_objects_to_lattice(hook, context.selected_objects)
		
		# Set up Radius control.
		add_radius_constraint(hook, hook, root)
		add_radius_constraint(lattice_ob, hook, root)

		root_drv = simple_driver(root, 'empty_display_size', hook, '["Radius"]')
		root_drv.expression = 'var/2'

		# Deselect everything, select the hook and make it active
		bpy.ops.object.select_all(action='DESELECT')
		hook.select_set(True)
		context.view_layer.objects.active = hook

		return {'FINISHED'}

class TWEAKLAT_OT_Falloff(bpy.types.Operator):
	"""Adjust falloff of the hook vertex group of a Tweak Lattice"""
	bl_idname = "lattice.tweak_lattice_adjust_falloww"
	bl_label = "Adjust Falloff"
	bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

	def update(self, context):
		if self.doing_invoke: return
		hook = context.object
		lattice_ob = hook['Lattice']
		ensure_falloff_vgroup(lattice_ob, 'Hook', multiplier=self.multiplier)
		hook['Multiplier'] = self.multiplier

	# Actual parameters
	multiplier: FloatProperty(name="Multiplier", default=1, update=update, min=0, soft_max=2)

	# Storage to share info between Invoke and Update
	lattice_start_scale: FloatVectorProperty()
	hook_start_scale: FloatVectorProperty()
	doing_invoke: BoolProperty(default=True)

	@classmethod
	def poll(cls, context):
		ob = context.object
		return ob.type=='EMPTY' and 'Tweak Lattice' in ob

	def invoke(self, context, event):
		hook = context.object
		self.multiplier = hook['Multiplier']
		self.hook_start_scale = hook.scale.copy()
		lattice_ob = hook['Lattice']
		self.lattice_start_scale = lattice_ob.scale.copy()

		self.doing_invoke = False
		wm = context.window_manager
		return wm.invoke_props_dialog(self)

	def draw(self, context):
		layout = self.layout
		layout.use_property_split = True
		layout.use_property_decorate = False
		layout.prop(self, 'multiplier', text="Strength", slider=True)

	def execute(self, context):
		return {'FINISHED'}

class TWEAKLAT_OT_Delete(bpy.types.Operator):
	"""Delete a tweak lattice setup with all its helper objects, drivers, etc"""
	bl_idname = "lattice.delete_tweak_lattice"
	bl_label = "Delete Tweak Lattice"
	bl_options = {'REGISTER', 'UNDO'}

	@classmethod
	def poll(cls, context):
		ob = context.object
		return ob.type=='EMPTY' and 'Tweak Lattice' in ob

	def execute(self, context):
		hook = context.object
		lattice = hook['Lattice']
		root = hook['Root']

		# Remove Lattice modifiers and their drivers.
		remove_all_objects_from_lattice(hook)
		
		# Remove hook Action if exists.
		if hook.animation_data and hook.animation_data.action:
			bpy.data.actions.remove(hook.animation_data.action)
		
		# Remove objects and Lattice datablock.
		bpy.data.objects.remove(hook)
		lattice_data = lattice.data
		bpy.data.objects.remove(lattice)
		bpy.data.lattices.remove(lattice_data)
		bpy.data.objects.remove(root)

		# Remove the collection if it's empty.
		coll = bpy.data.collections.get(coll_name)
		if coll and len(coll.all_objects)==0:
			bpy.data.collections.remove(coll)


		return {'FINISHED'}

class TWEAKLAT_OT_Add_Objects(bpy.types.Operator):
	"""Add selected objects to this tweak lattice"""
	bl_idname = "lattice.add_selected_objects"
	bl_label = "Add Selected Objects"
	bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

	@classmethod
	def poll(cls, context):
		ob = context.object
		if ob.type!='EMPTY' or 'Tweak Lattice' not in ob: return False
		values = ob.values()
		for sel_o in context.selected_objects:
			if sel_o==ob or sel_o.type!='MESH': continue
			if sel_o not in values:
				return True
		return False

	def execute(self, context):
		hook = context.object

		# Add Lattice modifier to the selected objects
		add_objects_to_lattice(hook, context.selected_objects)

		return {'FINISHED'}

class TWEAKLAT_OT_Remove_Objects(bpy.types.Operator):
	"""Remove selected objects from this tweak lattice"""
	bl_idname = "lattice.remove_selected_objects"
	bl_label = "Remove Selected Objects"
	bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

	@classmethod
	def poll(cls, context):
		ob = context.object
		if ob.type!='EMPTY' or 'Tweak Lattice' not in ob: return False
		values = ob.values()
		for sel_o in context.selected_objects:
			if sel_o==ob or sel_o.type!='MESH': continue
			if sel_o in values:
				return True
		return False

	def execute(self, context):
		hook = context.object

		# Add Lattice modifier to the selected objects
		remove_objects_from_lattice(hook, context.selected_objects)

		return {'FINISHED'}


class TWEAKLAT_PT_Main(bpy.types.Panel):
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'UI'
	bl_category = 'Lattice Magic'
	bl_label = "Tweak Lattice"

	@classmethod
	def poll(cls, context):
		return context.object
	
	def draw(self, context):
		layout = self.layout
		layout.use_property_split = True
		layout.use_property_decorate = False

		hook = context.object
		layout = layout.column()
		if hook.type!='EMPTY' or 'Tweak Lattice' not in hook:
			scene = context.scene
			layout.row().prop(scene, 'tweak_lattice_location', expand=True)
			layout.prop(scene, 'tweak_lattice_radius', slider=True)
			layout.separator()
			layout.prop(scene, 'tweak_lattice_parent_ob')
			if scene.tweak_lattice_parent_ob and scene.tweak_lattice_parent_ob.type=='ARMATURE':
				layout.prop_search(scene, 'tweak_lattice_parent_bone', scene.tweak_lattice_parent_ob.data, 'bones')
			layout.separator()
			op = layout.operator(TWEAKLAT_OT_Create.bl_idname, icon='OUTLINER_OB_LATTICE')
			op.radius = scene.tweak_lattice_radius
			return

		layout.prop(hook, '["Tweak Lattice"]', slider=True, text="Influence")
		layout.prop(hook, '["Radius"]', slider=True)
		layout.operator(TWEAKLAT_OT_Falloff.bl_idname, text="Adjust Falloff")

		layout.separator()
		layout.operator(TWEAKLAT_OT_Delete.bl_idname, text='Delete Tweak Lattice', icon='TRASH')

		layout.separator()
		layout.label(text="Helper Objects")
		lattice_row = layout.row()
		lattice_row.prop(hook, '["Lattice"]', text="Lattice")
		lattice_row.prop(hook['Lattice'], 'hide_viewport', text="", emboss=False)

		root_row = layout.row()
		root_row.prop(hook, '["Root"]', text="Root")
		root_row.prop(hook['Root'], 'hide_viewport', text="", emboss=False)

		layout.separator()
		layout.label(text="Parenting")
		root = hook['Root']
		col = layout.column()
		col.enabled = False
		col.prop(root, 'parent')
		if root.parent and root.parent.type=='ARMATURE':
			col.prop(root, 'parent_bone')

		layout.separator()
		layout.label(text="Add Objects")
		for o in context.selected_objects:
			if o == hook or o.type!='MESH': continue
			if o in hook.values(): continue
			layout.label(text=o.name, icon='ADD')
		layout.operator(TWEAKLAT_OT_Add_Objects.bl_idname, icon='ADD')

		layout.separator()
		layout.label(text="Remove Objects")
		for o in context.selected_objects:
			if o == hook or o.type!='MESH': continue
			if o not in hook.values(): continue
			layout.label(text=o.name, icon='REMOVE')
		layout.operator(TWEAKLAT_OT_Remove_Objects.bl_idname, icon='REMOVE')

		layout.separator()
		layout.label(text="Affected Objects")

		ob_count = 0
		ob_prop_name = "object_"+str(ob_count)
		while ob_prop_name in hook:
			layout.prop(hook, f'["{ob_prop_name}"]', text="")
			ob_count += 1
			ob_prop_name = "object_"+str(ob_count)

classes = [
	TWEAKLAT_OT_Create
	,TWEAKLAT_OT_Delete
	,TWEAKLAT_OT_Falloff
	,TWEAKLAT_OT_Add_Objects
	,TWEAKLAT_OT_Remove_Objects
	,TWEAKLAT_PT_Main
]

def register():
	from bpy.utils import register_class
	for c in classes:
		register_class(c)
	
	bpy.types.Scene.tweak_lattice_radius = FloatProperty(name="Radius", default=0.1, min=0.0001, max=1000, soft_max=2)
	bpy.types.Scene.tweak_lattice_parent_ob = PointerProperty(type=bpy.types.Object, name="Parent")	# Maybe it would be safer to make this a StringProperty but whatever.
	bpy.types.Scene.tweak_lattice_parent_bone = StringProperty(name="Bone")
	bpy.types.Scene.tweak_lattice_location = EnumProperty(name="Location", items=[
		('CURSOR', "3D Cursor", "Create at the location and orientation of the 3D cursor.")
		,('CENTER', "Center", "Create at the bounding box center of all selected objects.")
	])

def unregister():
	from bpy.utils import unregister_class
	for c in reversed(classes):
		unregister_class(c)
	
	del bpy.types.Scene.tweak_lattice_radius
	del bpy.types.Scene.tweak_lattice_parent_ob
	del bpy.types.Scene.tweak_lattice_parent_bone
	del bpy.types.Scene.tweak_lattice_location